package com.loanstreet.interview.dto

import com.loanstreet.interview.model.Loan
import java.math.BigDecimal
import java.time.OffsetDateTime
import javax.validation.constraints.DecimalMin
import javax.validation.constraints.Digits
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

data class LoanDTO(

    var createdAt: OffsetDateTime?,

    var updatedAt: OffsetDateTime?,

    @field:Digits(fraction = 2, integer = 8)
    @field:DecimalMin(value = "0.00")
    var amount: String?,

    @field:Digits(fraction = 2, integer = 8)
    @field:DecimalMin(value = "0.00")
    var interestRate: String?,

    @field:Min(1)
    var durationInMonths: Int?,

    @field:Digits(fraction = 2, integer = 8)
    @field:DecimalMin(value = "0.00")
    var monthlyPaymentAmount: String?,

    var id: Long?,

    ) {
    companion object {
        fun fromModel(model: Loan): LoanDTO {
            return LoanDTO(
                createdAt = model.createdAt,
                updatedAt = model.updatedAt,
                amount = model.amount.toString(),
                interestRate = model.interestRate.toString(),
                durationInMonths = model.durationInMonths,
                monthlyPaymentAmount = model.monthlyPaymentAmount.toString(),
                id = model.id,
            )
        }

        fun toModel(dto: LoanDTO): Loan {
            return Loan(
                amount = BigDecimal(dto.amount!!),
                interestRate = BigDecimal(dto.amount!!),
                durationInMonths = dto.durationInMonths!!,
                monthlyPaymentAmount = BigDecimal(dto.monthlyPaymentAmount!!),
            )
        }
    }
}
