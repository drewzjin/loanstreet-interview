package com.loanstreet.interview.repository

import com.loanstreet.interview.model.Loan
import org.springframework.data.repository.CrudRepository
import java.util.*

interface LoanRepository : CrudRepository<Loan, Long> {
}