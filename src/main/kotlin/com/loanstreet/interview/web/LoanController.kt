package com.loanstreet.interview.web

import com.loanstreet.interview.dto.LoanDTO
import com.loanstreet.interview.exceptions.NotFoundException
import com.loanstreet.interview.repository.LoanRepository
import com.loanstreet.interview.service.LoanService
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
class LoanController(val loanRepository: LoanRepository, val loanService: LoanService) {

    @GetMapping("/api/v1/loans/{loanId}")
    fun get(@PathVariable loanId: Long): LoanDTO {
        val dbLoan = loanRepository.findById(loanId).orElseThrow { NotFoundException() }
        return LoanDTO.fromModel(dbLoan)
    }

    @PutMapping("/api/v1/loans/{loanId}")
    fun update(@PathVariable loanId: Long, @Valid @RequestBody newLoan: LoanDTO): LoanDTO {
        var updatedLoan = loanService.updateLoan(loanId, newLoan)

        return LoanDTO.fromModel(updatedLoan)
    }

    @PostMapping("/api/v1/loans")
    fun create(@Valid @RequestBody newLoan: LoanDTO): LoanDTO {
        val incomingLoan = LoanDTO.toModel(newLoan)
        val newLoan = loanService.createLoan(incomingLoan)

        return LoanDTO.fromModel(newLoan)
    }
}