package com.loanstreet.interview.service

import com.loanstreet.interview.dto.LoanDTO
import com.loanstreet.interview.exceptions.BadRequestException
import com.loanstreet.interview.exceptions.NotFoundException
import com.loanstreet.interview.model.Loan
import com.loanstreet.interview.repository.LoanRepository
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.time.OffsetDateTime

@Service
class LoanService(val loanRepository: LoanRepository) {
    fun updateLoan(loanId: Long, newLoan: LoanDTO): Loan {
        val dbLoan = loanRepository.findById(loanId).orElseThrow { NotFoundException() }
        dbLoan?.let { loan -> // selective updates for important fields

            newLoan.durationInMonths?.let { dbLoan.durationInMonths = it }
            newLoan.interestRate?.let { dbLoan.interestRate = BigDecimal(it) }
            newLoan.monthlyPaymentAmount?.let {
                loan.monthlyPaymentAmount = BigDecimal(it)
            }
            loan.updatedAt = OffsetDateTime.now()

            return loanRepository.save(dbLoan)
        }
        throw NotFoundException()
    }

    fun createLoan(loan: Loan): Loan {
        loan.id?.let {
            BadRequestException()
        }
        loan.createdAt = OffsetDateTime.now()
        return loanRepository.save(loan)
    }
}