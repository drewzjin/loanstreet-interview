package com.loanstreet.interview.model

import org.hibernate.annotations.GenericGenerator
import org.springframework.context.annotation.Primary
import java.math.BigDecimal
import java.time.OffsetDateTime
import java.util.*
import javax.persistence.*


@Entity
data class Loan(
    var createdAt: OffsetDateTime = OffsetDateTime.now(),
    var updatedAt: OffsetDateTime = OffsetDateTime.now(),

    @Column(precision = 10, scale = 2)
    var amount : BigDecimal,

    @Column(precision = 10, scale = 2)
    var interestRate: BigDecimal,

    @Column()
    var durationInMonths: Int,

    @Column(precision = 10, scale = 2)
    var monthlyPaymentAmount: BigDecimal,

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = 0

)