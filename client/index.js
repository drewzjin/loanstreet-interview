import axios from "axios";

class LoanClient {
    constructor(props) {
        this.baseURL = props?.baseURL ?? 'http://localhost:8080/api/v1'
    }

    create(loan) {
        return axios.post(this.baseURL + '/loans', loan)
    }

    update(loanId, loan) {
        return axios.put(`${this.baseURL}/loans/${loanId}`, loan)
    }

    get(loanId) {
        return axios.get(`${this.baseURL}/loans/${loanId}`)
    }
}

const client = new LoanClient()

// create two same loans, update the first one, and retrieve the rest
// loan object:
const myLoan = {
    amount: '1000.00',
    interestRate: '3.0', // in percent
    durationInMonths: 150,
    monthlyPaymentAmount: '10.00' // in dollar
}

const {data: createdLoan} = await client.create(myLoan)

const {data: createdLoan2} = await client.create(myLoan)
console.log('createdLoan', createdLoan)
console.log('createdLoan2', createdLoan2)

const partialUpdate = {
    durationInMonths: 20,
    monthlyPaymentAmount: '5'
}
const {data: updatedLoan} = await client.update(createdLoan.id, partialUpdate)

console.log('updated loan', updatedLoan)

const {data: retrievedLoan} = await client.get(updatedLoan.id)
const {data: retrievedLoan2} = await client.get(createdLoan2.id)

console.log('retrievedLoan', retrievedLoan)

console.log('retrievedLoan2', retrievedLoan2)

// form validations and responses

console.log('---')

const badLoan = {
    amount: '0',
    interestRate: '0',
    durationInMonths: 0,
    monthlyPaymentAmount: 0,
}


const badLoanMissingFields = {
    amount: '1000',
    interestRate: '10',
}


const badPartialUpdate = {
    interestRate: '0',
    durationInMonths: 0,
}

try {
    const badLoanCreate = await client.create(badLoan)
    console.log('badLoanCreate', badLoanCreate.data)
    console.log('should not see this')
} catch (e) {
    console.log('responds 400 for bad create', e.response.status === 400)
}

try {
    const badLoanCreateMissingFields = await client.create(badLoanMissingFields)
    console.log('badLoanMissingFields', badLoanCreateMissingFields.data)
    console.log('should not see this')
} catch (e) {
    console.log('responds 400 for bad create', e.response.status === 400)
}


try {
    await client.update(createdLoan.id, badPartialUpdate)
    console.log('should not see this')
} catch (e) {
    console.log('responds 400 for bad partial update', e.response.status === 400)
}

try {
    await client.get(100000001)
} catch (error) {
    console.log('responds 404 for missing get', error.response.status === 404)
}
try {
    await client.update(100000001, partialUpdate)
} catch (error) {
    console.log('responds 404 for missing update', error.response.status === 404)
}


