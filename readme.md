Languages: Kotlin & Javascript

How to run:
```
# database 
docker-compose up -d

# app
./gradlew bootRun

# client (needs axios)
cd client && npm i

# inside 'client' directory

node index.js
```
Client covers:

Happy path:
- create two loans (incrementing ids)
- update one with partial payload
- retrieve both

Errors:
- bad requests (object validations) should 400
- missing primary key (db validation) should 404


